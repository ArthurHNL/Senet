package nl.arthurheidt.senet;

import java.util.HashMap;

/**
 * A game of Senet
 * 
 * @author Arthur Heidt
 */
public class Game {
	// Set this to true if you want to enter what you throw, instead of randomly
	// throwing dice.
	public final boolean cheat = false;

	private Player[] players;
	private HashMap<Integer, String> boards;
	private Board board;
	private UserInteractor interactor;
	private Dice dice;

	public Game() {
		// Create objects
		dice = new Dice();
		interactor = new UserInteractor();
		players = new Player[2];
		boards = new HashMap<Integer, String>();

		// Put the default boards on the map.
		boards.put(0, "oxoxoxoxo.x...................");
		boards.put(1, "xo.o.o.x.o.o.o.xoo.oo.xooo....");
		boards.put(2, ".....................ooo....x.");
		boards.put(3, ".....o......x....o...o..xx.xx.");
	}

	/**
	 * Play a game of Senet.
	 */
	public void playGame() {
		// Print splashtext
		System.out.println("Welcome to Senet!");
		System.out.println();

		// Get the type of board
		int customBoard = boards.size();
		int maxDefBoard = customBoard - 1;
		System.out.println("Do you want to play a normal game (0), a predefined testgame (1-" + maxDefBoard
				+ "), or a custom game (" + customBoard + ", advanced users only).");
		int boardChoice = 0;
		while (true) {
			boardChoice = interactor.getInt();
			if (boardChoice >= 0 && boardChoice <= customBoard) {
				break;
			}
			System.out.println("Invalid input! Input must be between 0 and " + customBoard + "!");
		}
		String boardString;
		if (boardChoice == customBoard) {
			System.out.println("Please enter your custom board, eg: \"oxoxoxoxo.x...................\".");
			while (true) {
				boardString = interactor.getString();
				if (boardString.length() != 30 || !boardString.matches("^[.xo]+$")) {
					System.out.println("Invalid input!");
					continue;
				}
				break;
			}
		} else {
			boardString = boards.get(boardChoice);
		}
		System.out.println();

		// Create a board
		board = new Board(boardString);

		// Setup players
		players[0] = new Player(board, dice, 1, interactor);
		players[1] = new Player(board, dice, 2, interactor);
		if (cheat) {
			System.out.println("!!!!!!CHEAT MODE IS ON!!!!!!");
			for (Player p : players) {
				p.enableCheat();
			}
		}

		// If we're using the default board, play the first phase of the game. Else,
		// skip it.
		if (boardChoice == 0) {
			firstPlayer: while (true) {
				for (int i = 0; i < players.length; i++) {
					int thrown = dice.throwPoints();
					System.out.println(players[i].getName() + " throws " + thrown + ".");
					if (thrown == 1) {
						System.out.println(players[i].getName() + " is now black [x].");
						players[i].setColorSign('x');
						if (i == 0) {
							players[1].setColorSign('o');
						} else {
							players[0].setColorSign('o');
							// Swap the players
							Player cache = players[0];
							players[0] = players[1];
							players[1] = cache;
						}
						break firstPlayer;
					}
				}
			}
			// Make white force a move on spot 9
			players[1].doMove(9);
		} else {
			System.out.println("The first stage of the game was skipped, black (x) may now make his move.");
			players[0].setColorSign('x');
			players[1].setColorSign('o');
		}

		Player winner;
		// Main loop of the game.
		mainGame: while (true) {
			for (Player p : players) {
				while (true) {
					boolean result = p.doMove(0);
					if (p.isWinner()) {
						winner = p;
						break mainGame;
					}
					if (!result) {
						break;
					}
				}
			}
		}
		System.out.println();
		System.out.println("Congratulations " + winner.getName() + ", you won the game!");

	}
}