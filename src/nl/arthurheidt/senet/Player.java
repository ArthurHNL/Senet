package nl.arthurheidt.senet;

/**
 * A player for a Senet game
 * @author Arthur Heidt
 *
 */
public class Player {
	private Board board;
	private Dice dice;
	private char colorSign;
	private String name;
	private UserInteractor ui;
	private boolean cheat;

	public Player(Board b, Dice d, int playerNumber, UserInteractor u) {
		setBoard(b);
		setDice(d);
		ui = u;
		System.out.println("Player " + playerNumber + ", please enter your name.");
		setName(u.getString());
		cheat = false;
	}

	public void enableCheat() {
		cheat = true;
	}

	public void setBoard(Board b) {
		board = b;
	}

	public Board getBoard() {
		return board;
	}

	public void setDice(Dice d) {
		dice = d;
	}

	public Dice getDice() {
		return dice;
	}

	public void setColorSign(char c) throws IllegalArgumentException {
		if (c != 'x' && c != 'o') {
			throw new IllegalArgumentException("Colorsign must be 'x' or 'o'!");
		}
		colorSign = c;
	}

	public char getColorSign() {
		return colorSign;
	}

	public void setName(String n) throws IllegalArgumentException {
		if (n.isEmpty()) {
			throw new IllegalArgumentException("Name can not be empty!");
		}
		name = n;
	}

	public String getName() {
		return name;
	}

	/**
	 * Make a move
	 * 
	 * @param pos
	 *            position to move. 0 for free choice.
	 * @return true if we want another move, false if not.
	 */
	public boolean doMove(int pos) {
		boolean anotherMove = false;
		board.print();
		boolean moveForward = true;

		if (colorSign != 'x' && colorSign != 'o') {
			System.out.println("Can not make a move when colorSign has not been determined!");
			return false;
		}
		int points;
		if (cheat) {
			while (true) {
				System.out.println("[CHEAT] " + getName() + " [" + getColorSign()
						+ "], please enter the number of points you want to \"throw\"...");
				points = ui.getInt();
				if (points > 0 && points <= 6 && points != 5) {
					break;
				}
				System.out.println("Invalid Input!");
			}
		} else {
			System.out.println(getName() + " [" + getColorSign() + "], please press <ENTER> to throw dice...");
			ui.pause();
			points = dice.throwPoints();
			System.out.println("You threw " + points + " points.");
		}
		
		if (points == 1 || points == 4 || points == 6) {
			anotherMove = true;
		}
		
		if (!board.movingPossible(true, points, colorSign)) {
			if (!board.movingPossible(false, points, colorSign)) {
				System.out.println("No moves possible! Passing!");
				System.out.println();
				return anotherMove;
			} else {
				System.out.println("No forward moves possible! Now moving backwards!");
				moveForward = false;
			}
		}
		// If we're not forced to move a certain position
		if (pos == 0) {
			while (true) {
				System.out.println(getName() + " [" + getColorSign() + "], which piece do you want to move?");
				pos = ui.getInt();
				if (pos < 1 || pos > 30) {
					System.out.println("Invalid input! Input must be a valid position! (1-30)");
					continue;
				}
				if (board.doMove(moveForward, points, pos, colorSign)) {
					System.out.println();
					return anotherMove;
				}
			}
		} else {
			board.doMove(moveForward, points, pos, colorSign);
			return anotherMove;
		}

	}
	
	/**
	 * Checks if we are the winner.
	 */
	public boolean isWinner() {
		return board.isWinner(colorSign);
	}

}