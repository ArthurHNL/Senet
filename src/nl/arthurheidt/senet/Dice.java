package nl.arthurheidt.senet;

import java.util.Random;

/**
 * 
 * A "dice" made of slats, to use in a senet game.
 * 
 * @author Arthur Heidt
 */
public class Dice {
	private Random rnd;

	/**
	 * Creates a new dice object.
	 */
	public Dice() {
		rnd = new Random();
	}

	/**
	 * Throws the 4 slats in the air, and calculates the points.
	 * 
	 * @return Points to be used by the game.
	 */
	public int throwPoints() {
		int numWhite = 0;
		// Simulate 4 throws
		for (int i = 0; i < 4; i++) {
			if (rnd.nextBoolean()) {
				numWhite++;
			}
		}
		// If there are 0 white (aka all black), give 6 points
		if (numWhite == 0) {
			return 6;
		}
		// Else give the number of white as points
		return numWhite;
	}

}
