package nl.arthurheidt.senet;

import java.util.ArrayList;

/**
 * A Senet board
 * @author Arthur Heidt
 * @version 0.1
 */

/**
 * A board for Senet.
 * 
 * @author Arthur Heidt
 */
public class Board {
	private char[] squares;
	ArrayList<Integer> safeSpots;
	ArrayList<Integer> traps;
	private final static String defaultBoard = "oxoxoxoxo.x...................";

	/**
	 * Create a board with custom start positions
	 * 
	 * @param squareString
	 *            Start positions, e.g.: "xo.o.o.x.oo.oox.o.o.o.xooo....".
	 */
	public Board(String squareString) {
		// Create a list of safespots
		safeSpots = new ArrayList<Integer>();
		safeSpots.add(26 - 1);
		safeSpots.add(28 - 1);
		safeSpots.add(29 - 1);

		// Create a list of traps
		traps = new ArrayList<Integer>();
		traps.add(27 - 1);

		// Check if the specified string is compatible
		if (squareString.length() != 30 || !squareString.matches("^[.xo]+$")) {
			throw new IllegalArgumentException("Invalid Field input!");
		}

		// Make the field based on the sepcified string
		squares = squareString.toCharArray();
	}

	/**
	 * Create a board with the default start positions
	 */
	public Board() {
		this(defaultBoard);
	}

	/**
	 * Gets the length of the board (aka length of the internal char array)
	 * 
	 * @return length of the board
	 */
	public int getLength() {
		return squares.length;
	}

	/**
	 * Print out the board.
	 */
	public void print() {
		System.out.println("+----------+");
		for (int i = 0; i < 3; i++) {
			int begin = i * 10;
			int end = (i + 1) * 10;
			if (i % 2 == 0) {
				printForwards(begin, end);
			} else {
				printBackwards(begin, end);
			}
		}
		System.out.println("+----------+");
	}

	/**
	 * Private method to print ->
	 */
	private void printForwards(int start, int stop) {
		System.out.print("|");
		for (int i = start; i < stop; i++) {
			System.out.print(squares[i]);
		}
		System.out.print("|");
		System.out.println();
	}

	/**
	 * Private method to print <-
	 */
	private void printBackwards(int start, int stop) {
		System.out.print("|");
		for (int i = (stop - 1); i >= start; i--) {
			System.out.print(squares[i]);
		}
		System.out.print("|");
		System.out.println();
	}

	/**
	 * Method that will check if a move is valid
	 * 
	 * @param points
	 *            number of points from the dice
	 * @param pos
	 *            position of the
	 * @param colorSign
	 *            colorsign of the player
	 * @param isForward
	 *            true if the move is forward.
	 * @param printErrors
	 *            true if you want the error message to be printed to the console.
	 * @return true if move is valid, false if invalid
	 */
	public boolean moveIsPossible(int points, int pos, char colorSign, boolean isForward, boolean printErrors) {
		int pcpos = pos - 1;

		if (!isForward) {
			points = -points;
		}

		// Get the position of the square we want to move to
		int wantedPos = pcpos + points;

		// Return false IF wanted position exceeds the length of the board
		if (wantedPos > (squares.length - 1) || wantedPos < 0) {
			if (printErrors) {
				System.out.println("Invalid move: trying to move out of the board.");
			}
			return false;
		}

		// Define friend and foe
		char friend = colorSign;
		char foe;
		if (friend == 'x') {
			foe = 'o';
		} else {
			foe = 'x';
		}

		// Check if we're not trying to move a foe, or an empty space
		if (squares[pcpos] == foe) {
			if (printErrors) {
				System.out.println("Invalid move: trying to move a piece that does not belong to you.");
			}
			return false;
		}
		if (squares[pcpos] == '.') {
			if(printErrors) {
				System.out.println("Invalid move: there is no piece there...");
			}
			return false;
		}

		// Check if we're not trying to attack a friend
		if (squares[wantedPos] == friend) {
			if (printErrors) {
				System.out.println("Invalid move: friendly fire.");
			}
			return false;
		}

		// Check if we're not moving over 3 or more foe's
		int countFoes = 0;
		int start;
		int stop;
		if (isForward) {
			start = pos;
			stop = wantedPos;
		} else {
			start = wantedPos;
			stop = pos;
		}
		for (int i = start; i < stop; i++) {
			if (squares[i] == foe) {
				countFoes++;
			} else {
				countFoes = 0;
			}
			if (countFoes >= 3) {
				if(printErrors) {
				System.out.println(
						"Invalid move: trying to jump over 3 or more foes. One of them is positioned at " + i + ".");
				}
				return false;
			}
		}
		
		//IF we are going to the finish, check IF all our pieces are on the last line
		if (wantedPos == (squares.length - 1)) {
			for (int i = 0; i < (squares.length - 10); i++) {
				if (squares[i] == friend) {
					if (printErrors) {
						System.out.println("Can't finish a piece when not all you're pieces are on the last row (" + (i + 1) + ").");
					}
					return false;
				}
			}
		}

		// Check if the space is empty
		if (squares[wantedPos] == '.') {
			return true;
		}
		// We're still in the method, which means we're attacking a foe.
		// Check if the foe is on a safe spot
		if (safeSpots.contains(wantedPos)) {
			if(printErrors) {
			System.out.println("Invalid move: trying to attack a foe on a safe spot.");
			}
			return false;
		}
		// Check if the foe we're attacking is not outnumbering us
		if (wantedPos != 0) {
			int i = wantedPos - 1;
			if (squares[i] == foe) {
				if (printErrors) {
					System.out
							.println("Invalid move: you are trying to attack a foe that is outnumbering you (piece on "
									+ i + ").");
				}
				return false;
			}
		}
		if (wantedPos != (squares.length - 1)) {
			int i = wantedPos + 1;
			if (squares[i] == foe) {
				if (printErrors) {
					System.out
							.println("Invalid move: you are trying to attack a foe that is outnumbering you (piece on "
									+ i + ").");
				}
				return false;
			}
		}

		// We've made it here, so the move is safe!
		return true;

	}

	/**
	 * Check if it is possible to make a move
	 * 
	 * @param forwardMoves
	 *            True if you want to check for forward moves, false for backward
	 *            moves.
	 * @param points
	 *            points of the player
	 * @param colorSign
	 *            Color sign of the player.
	 * @return IF it is possible to make (specified forwards or backwards) move.
	 */
	public boolean movingPossible(boolean forwardMoves, int points, char colorSign) {
		for (int i = 1; i <= squares.length; i++) {
			if (moveIsPossible(points, i, colorSign, forwardMoves, false)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Make a move on the board.
	 * 
	 * @param isForward
	 *            true IF move is forward, false if backwards
	 * @param points
	 *            number of points
	 * @param pos
	 *            current position (human form)
	 * @param colorSign
	 *            colorsign of the player.
	 * @return
	 */
	public boolean doMove(boolean isForward, int points, int pos, char colorSign) {
		// Check if the move is correct
		if (moveIsPossible(points, pos, colorSign, isForward, true)) {
			// Let's make the move! Set up some variables:
			int pcpos = pos - 1;
			int wantedPos;
			if (isForward) {
				wantedPos = pcpos + points;
			} else {
				wantedPos = pcpos - points;
			}

			// Check if we're moving in a trap
			if (traps.contains(wantedPos)) {
				System.out.println("OOPS! You fell into a trap!");
				for (int i = 0; i < squares.length; i++) {
					if (squares[i] == '.') {
						wantedPos = i;
						break;
					}
				}
			}

			// Check if we're reaching the finish line
			if (wantedPos == (squares.length - 1)) {
				System.out.println("Another piece of the board!");
				squares[pcpos] = '.';
				return true;
			}
			// Move the pieces
			char cache = squares[wantedPos];
			squares[wantedPos] = squares[pcpos];
			squares[pcpos] = cache;

			return true;
		} else {
			return false;
		}
	}
	/**
	 * Check if a player has won the game.
	 * @param colorSign colorsign of the player
	 * @return true if the player has won the game
	 */
	public boolean isWinner(char colorSign) {
		if (!String.valueOf(squares).contains(String.valueOf(colorSign))) {
			return true;
		}
		return false;
	}

}
