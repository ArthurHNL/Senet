package nl.arthurheidt.senet;

/**
 * Launcher for a game of Senet
 * @author Arthur Heidt
 *
 */
public class Main {
	public static void main(String[] args) {
		Game g = new Game();
		g.playGame();
	}
}
