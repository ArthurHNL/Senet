package nl.arthurheidt.senet;

import java.util.Scanner;

/**
 * A generic interaction object that can ask questions to the user.
 * @author Arthur Heidt
 *
 */
public class UserInteractor {
	private Scanner scan;
	public UserInteractor() {
		scan = new Scanner(System.in);
	}
	public void finalize() {
		scan.close();
	}
	
	
	/**
	 * Gets a boolean input from the user.
	 * 
	 * @param defaultChoice
	 *            The default choice if no answer is given. 0 for no default (ask
	 *            again), 1 for yes and 2 for no.
	 * @return boolean input from the user.
	 */
	public boolean getBoolean(int defaultChoice) {
		String question;
		if (defaultChoice == 0) {
			question = "[y/n]";
		} else if (defaultChoice == 1) {
			question = "[Y/n]";
		} else {
			question = "[y/N]";
		}
		while (true) {
			System.out.println(question + "?");
			String input = scan.nextLine();
			if (input.isEmpty()) {
				if (defaultChoice == 1) {
					return true;
				} else if (defaultChoice == 2) {
					return false;
				} else {
					System.out.println("Invalid input! type Y (or y) for yes, and N (or n) for no!");
					continue;
				}
			}
			if (input.toLowerCase().equals("y")) {
				return true;
			} else if (input.toLowerCase().equals("n")) {
				return false;
			} else {
				System.out.println("Invalid input! type Y (or y) for yes, and N (or n) for no!");
				continue;
			}
		}
	}

	/**
	 * Gets a String input from the user. Does not allow empty input.
	 * 
	 * @return String input from the user.
	 */
	public String getString() {
		while (true) {
			System.out.println("Please enter your anwser, then hit <ENTER>");
			String input = scan.nextLine();
			if (input.isEmpty()) {
				System.out.println("Answer can not be empty!");
				continue;
			}
			return input;
		}
	}

	/**
	 * Gets an int input from the user. Does not allow empty or invalid input.
	 * @return int input from the user.
	 */
	public int getInt() {
		while(true) {
			System.out.println("Please enter your answer, then hit <ENTER>");
			int input;
			try {
				input = Integer.parseInt(scan.nextLine());
			} catch (NumberFormatException ex) {
				System.out.println("Invalid input! Your input must be a whole number!");
				continue;
			}
			return input;
		}
	}
	
	/**
	 * Pause the current thread until the user inputs a carriage return.
	 */
	public void pause() {
		scan.nextLine();
	}
}
